FROM node:12

WORKDIR /usr/app

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

ENV HOST 0.0.0.0

EXPOSE 3002

CMD ["npm", "run", "dev"]
