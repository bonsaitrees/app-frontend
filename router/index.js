import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

export function createRouter(ssrContext, createDefaultRouter, routerOptions) {
    const options = routerOptions || createDefaultRouter(ssrContext).options;
    const hostname = ssrContext ? ssrContext.req.headers.host : location.host;

    let routes = fixRoutes(options.routes, hostname);

    return new Router({
        ...options,
        routes: routes
    });
}
function fixRoutes(defaultRoutes, hostname) {
    let hostnameParts = hostname.split('.')

    if (hostnameParts.length === 3) {
        if (hostnameParts[0] === 'app') return subdomainRouteMatcher(defaultRoutes, 'app');
        if (hostnameParts[0] === 'blog') return subdomainRouteMatcher(defaultRoutes, 'blog');
        if (hostnameParts[0] === 'shop') return subdomainRouteMatcher(defaultRoutes, 'shop');
    }
    return filterDefaultRoutes(defaultRoutes);
}
function filterDefaultRoutes(defaultRoutes) {
    return defaultRoutes.filter(r => {
        return !(r.name.startsWith('app')
                ||
            r.name.startsWith('blog')
            ||
            r.name.startsWith('shop'));
    });
}
function subdomainRouteMatcher(defaultRoutes, subdomain) {
    return defaultRoutes.filter(route => {

        if (route.path === '/' + subdomain) {
            route.path = '/';
        } else {
            route.path = route.path.slice(subdomain.length + 1);
        }
        return route.name.startsWith(subdomain);
    });
}
